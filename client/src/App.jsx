import React from 'react';
import {Route} from 'react-router-dom';
import {connect} from 'react-redux';
import './App.css';
import CRUD from './components/CRUD/CRUD.jsx';


const App = (props) =>{
	return (
			<div className="App">
                <CRUD/>
			</div>
  );
}

export default App;

