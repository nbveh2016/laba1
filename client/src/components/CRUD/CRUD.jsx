import React from 'react';
import * as axios from 'axios';
import s from './CRUD.module.css';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import {NavLink, Route, withRouter} from 'react-router-dom';

import {createPdf} from './PDFDoc.js';

const request = (Obj, Metod, Data) => {
	return (
        axios.post("/api" + "/" + Obj + "/" + Metod, {
            Data: Data
        })
        .then((response) => {
          if(response.data.message) alert(response.data.message)
          return response.data
        })
        .catch(error => console.log(error))
    );
}

var tableToExcel = (csvData, fileName) => {
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx';
    
    const ws = XLSX.utils.json_to_sheet(csvData);
    const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
    const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    const data = new Blob([excelBuffer], {type: fileType});
    FileSaver.saveAs(data, fileName + fileExtension);
};

class Select extends React.Component {
    constructor(props){
        super(props);

        let custs = []
        for (let i = 0; i <= 25; i++) {
          custs.push({firstName: `first${i}`, lastName: `last${i}`,
          email: `abc${i}@gmail.com`, address: `000${i} street city, ST`, zipcode: `0000${i}`});
        }

        this.state = {
            Data: null,
            Custs: custs
        }
    }
    componentDidMount(){
        request(this.props.Table, "SELECT")
        .then((data) => {
            this.setState({
                Data: Object.values(data.Data).length? Object.values(data.Data): null
            });
        });
    }
    
	render(){
    return (
      <div>
        <table  className={"Table"}>
                    <thead>
                        <tr key={"key_tr"}>
                            {this.state.Data && Object.keys(this.state.Data[0]).map((el, ind) => {
                                return (<th key={ind}>{el}</th>)
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.Data && this.state.Data.map((el, ind) => {
                            return (
                                <tr key={ind}>
                                    {Object.values(el).map((el1, ind1) => {
                                        return (
                                            <td key={ind1}>{el1}</td>
                                        )
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
        <button onClick={() => {
          tableToExcel(this.state.Data, this.props.Table);
        }}>To excel</button>
        <button onClick={() => {
          createPdf(this.state.Data);
        }}>To pdf</button>
    </div>
   )
  }
}
class Search extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Data: null,
            value: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    }
    handleSubmit() {
        request(this.props.Table, "SEARCH", {Id: this.state.value})
        .then((data) => {
            this.setState({
                Data: Object.values(data.Data).length? Object.values(data.Data): null
            });
        });
    }
    
	render(){
        return (
            <div>
                <div>
                    <input type="text" value={this.state.value} onChange={this.handleChange} />
                    <button onClick={this.handleSubmit}>Search</button>
                </div>
                <table>
                    <thead>
                        <tr key={"key_tr"}>
                            {this.state.Data && Object.keys(this.state.Data[0]).map((el, ind) => {
                                return (<th key={ind}>{el}</th>)
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.Data && this.state.Data.map((el, ind) => {
                            return (
                                <tr key={ind}>
                                    {Object.values(el).map((el1, ind1) => {
                                        return (
                                            <td key={ind1}>{el1}</td>
                                        )
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
	   )
    }
}
class Insert extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Data: null
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        
        this.props.Form.forEach(el => {
            this.state = {...this.state, [el.Name + "Current"]: "", [el.Name + "Select"]: null};
        });
    }
    
    componentDidMount(){
        this.props.Form.forEach(el => {
            if(el.Select){
                request(el.Select, "SELECT")
                .then((data) => {
                    this.setState({
                        [el.Name + "Current"]: Object.values(data.Data).length? Object.values(data.Data)[0]["Id"]: null,
                        [el.Name + "Select"]: Object.values(data.Data).length? Object.values(data.Data): null
                    });
                });
            }
        });
    }
    
    handleSubmit(event) {
        console.log(this.state);
        let Data = {};
        this.props.Form.forEach(el => {
            Data = {...Data, [el.Name]: this.state[el.Name + "Current"]};
        });
        request(this.props.Table, "INSERT", Data);
        event.preventDefault();
    }
    
	render(){
        return (
            <div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        {
                        this.props.Form.map((el, ind) => {
                            if(el.Select){
                                return(
                                    <div>
                                        {el.Name}
                                        <select key={ind} onChange={(event) => this.setState({[el.Name + "Current"]: event.target.value})}>
                                            {this.state[el.Name + "Select"] && (this.state[el.Name + "Select"]).map((el1, ind1) => {
                                               return (
                                                   <option key={ind1} value={el1.Id}>{el1.Name? el1.Name: el1.Id}</option>
                                               )
                                            })}
                                        </select>
                                    </div>
                                )
                            }else{
                                return(
                                    <div>
                                        {el.Name}
                                        <input key={ind} type="text" value={this.state[el.Name + "Current"]} 
                                            onChange={(event) => this.setState({[el.Name + "Current"]: event.target.value})}
                                            required
                                        />
                                    </div>
                                    
                                )
                            }
                        })
                        }
                        <input type="submit" value="Insert" />
                    </form>
                    
                </div>
            </div>
	   )
    }
}
class Updata extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Data: null,
            Id: ""
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        
        this.props.Form.forEach(el => {
            this.state = {...this.state, [el.Name + "Current"]: "", [el.Name + "Select"]: null};
        });
    }
    
    componentDidMount(){
        this.props.Form.forEach(el => {
            if(el.Select){
                request(el.Select, "SELECT")
                .then((data) => {
                    this.setState({
                        [el.Name + "Current"]: Object.values(data.Data).length? Object.values(data.Data)[0]["Id"]: null,
                        [el.Name + "Select"]: Object.values(data.Data).length? Object.values(data.Data): null
                    });
                });
            }
        });
    }
    
    handleSubmit(event) {
        console.log(this.state);
        let Data = {Id: this.state.Id};
        this.props.Form.forEach(el => {
            Data = {...Data, [el.Name]: this.state[el.Name + "Current"]};
        });
        request(this.props.Table, "UPDATE", Data);
        event.preventDefault();
    }
    
	render(){
        return (
            <div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <div>
                            Id
                            <input type="text" value={this.state.Id} 
                                onChange={(event) => this.setState({Id: event.target.value})}
                                required
                            />
                        </div>
                        
                        {
                        this.props.Form.map((el, ind) => {
                            if(el.Select){
                                return(
                                    <div>
                                        {el.Name}
                                        <select key={ind} onChange={(event) => this.setState({[el.Name + "Current"]: event.target.value})}>
                                            {this.state[el.Name + "Select"] && (this.state[el.Name + "Select"]).map((el1, ind1) => {
                                               return (
                                                   <option key={ind1} value={el1.Id}>{el1.Name? el1.Name: el1.Id}</option>
                                               )
                                            })}
                                        </select>
                                    </div>
                                )
                            }else{
                                return(
                                    <div>
                                        {el.Name}
                                        <input key={ind} type="text" value={this.state[el.Name + "Current"]} 
                                            onChange={(event) => this.setState({[el.Name + "Current"]: event.target.value})}
                                            required
                                        />
                                    </div>
                                    
                                )
                            }
                        })
                        }
                        <input type="submit" value="Updata" />
                    </form>
                    
                </div>
            </div>
	   )
    }
}
class Delete extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Data: null,
            value: "1"
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    }
    handleSubmit() {
        request(this.props.Table, "DELETE", {Id: this.state.value});
    }
    
	render(){
        return (
            <div>
                <div>
                    <input type="text" value={this.state.value} onChange={this.handleChange} />
                    <button onClick={this.handleSubmit}>Delete</button>
                </div>
            </div>
	   )
    }
}

class Tools extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            CurrentTool: "SELECT"
        }
    }
    
	render(){
        return (
            <div>
                <NavLink to={"/" + this.props.Table + "/SELECT"} onClick={()=> this.setState({CurrentTool: "SELECT"})}>SELECT </NavLink>
                <NavLink to={"/" + this.props.Table + "/SEARCH"} onClick={()=> this.setState({CurrentTool: "SEARCH"})}>SEARCH </NavLink>
                <NavLink to={"/" + this.props.Table + "/INSERT"} onClick={()=> this.setState({CurrentTool: "INSERT"})}>INSERT </NavLink>
                <NavLink to={"/" + this.props.Table + "/UPDATE"} onClick={()=> this.setState({CurrentTool: "UPDATE"})}>UPDATE </NavLink>
                <NavLink to={"/" + this.props.Table + "/DELETE"} onClick={()=> this.setState({CurrentTool: "DELETE"})}>DELETE </NavLink>
                
                <Route path={"/" + this.props.Table + "/SELECT"}>
                    <Select Table={this.props.Table}/>
                </Route>
                <Route path={"/" + this.props.Table + "/SEARCH"}>
                    <Search Table={this.props.Table}/>
                </Route>
                <Route path={"/" + this.props.Table + "/INSERT"}>
                    <Insert Table={this.props.Table} Form={this.props.Form}/>
                </Route>
                <Route path={"/" + this.props.Table + "/UPDATE"}>
                    <Updata Table={this.props.Table} Form={this.props.Form}/>
                </Route>
                <Route path={"/" + this.props.Table + "/DELETE"}>
                    <Delete Table={this.props.Table}/>
                </Route>
            </div>
	   )
    }
}

export default class CRUD extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            Table: "Product",
            Form: [{Name: "Name", Select: null}, {Name: "IdCat", Select: "Category"}, {Name: "IdUnit", Select: "Unit"}]
        }
    }
    
	render(){
        return (
            <div className={s.CRUD}>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Category",
                            Form: [{Name: "Name", Select: null}]
                        })}
                        to={"/Category"}>Category</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Delivery",
                            Form: [{Name: "Date", Select: null}, {Name: "IdWorkman", Select: "Workman"}, {Name: "IdDistributor", Select: "Distributor"}]
                        })}
                        to={"/Delivery"}>Delivery</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "DeliveryComposition",
                            Form: [{Name: "IdDelivery", Select: "Delivery"}, {Name: "IdProduct", Select: "Product"}, {Name: "Number", Select: null}]
                        })}
                        to={"/DeliveryComposition"}>DeliveryComposition</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Distributor",
                            Form: [{Name: "Name", Select: null}]
                        })}
                        to={"/Distributor"}>Distributor</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Position",
                            Form: [{Name: "Name", Select: null}]
                        })}
                        to={"/Position"}>Position</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Product",
                            Form: [{Name: "Name", Select: null}, {Name: "IdCategory", Select: "Category"}, {Name: "IdUnit", Select: "Unit"}]
                        })}
                        to={"/Product"}>Product</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "ProductDistributor",
                            Form: [{Name: "IdProduct", Select: "Product"}, {Name: "IdDistributor", Select: "Distributor"}]
                        })}
                        to={"/ProductDistributor"}>ProductDistributor</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "ProductPosition",
                            Form: [{Name: "IdProduct", Select: "Product"}, {Name: "IdPosition", Select: "Position"}]
                        })}
                        to={"/ProductPosition"}>ProductPosition</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Unit",
                            Form: [{Name: "Name", Select: null}]
                        })}
                        to={"/Unit"}>Unit</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Workman",
                            Form: [{Name: "Name", Select: null}, {Name: "IdPosition", Select: "WorkmanPosition"}]
                        })}
                        to={"/Workman"}>Workman</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "WorkmanPosition",
                            Form: [{Name: "Name", Select: null}]
                        })}
                        to={"/WorkmanPosition"}>WorkmanPosition</NavLink>
                <Tools Table={this.state.Table} Form={this.state.Form}/>
            </div>
	   )
    }
}
