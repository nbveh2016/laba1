import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export async function createPdf(Data) {
  var docDefinition = {
    pageSize: {
      width: 600,
      height: 'auto'
    },
    content: [
      "Table",
      {
        layout: 'lightHorizontalLines', // optional
        table: {
          headerRows: 1,
          widths: [ '*', '*', '*', '*' ],
          body: [
          ]
        }
      }
    ]
  }
  
  let temp = []
  Object.keys(Data[0]).forEach(el => {
    temp.push(el)
  });
  console.log(temp)
  
  docDefinition.content[1].table.body.push(temp);

  
  Data.forEach(el => {
    docDefinition.content[1].table.body.push(Object.values(el));
  });
  
  pdfMake.createPdf(docDefinition).open();
}