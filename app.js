const express = require("express")
const config = require("config")
const app = new express()

const PORT = config.get("port") || 5000

app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.use('/api/Category', require('./routes/Category'))
app.use('/api/Delivery', require('./routes/Delivery'))
app.use('/api/DeliveryComposition', require('./routes/DeliveryComposition'))
app.use('/api/Distributor', require('./routes/Distributor'))
app.use('/api/Position', require('./routes/Position'))
app.use('/api/Product', require('./routes/Product'))
app.use('/api/ProductDistributor', require('./routes/ProductDistributor'))
app.use('/api/ProductPosition', require('./routes/ProductPosition'))
app.use('/api/Unit', require('./routes/Unit'))
app.use('/api/Workman', require('./routes/Workman'))
app.use('/api/WorkmanPosition', require('./routes/WorkmanPosition'))

async function start() {
  try{
    app.listen(PORT, () => console.log(`Starn in port ${PORT}`))
  } catch (e) {
    console.log('Server Error', e.message)
    process.exit(1)
  }
}

start()